package com.twuc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class BowlingScoreCalculatorTest {
    private BowlingScoreCalculator bowlingScoreCalculator;

    @BeforeEach
    void setUp() {
        bowlingScoreCalculator = new BowlingScoreCalculator();
    }

    @Test
    void should_get_0_when_no_throw() {
        int score = bowlingScoreCalculator.getTotalScore();
        int expected = 0;
        assertEquals(expected, score);
    }

    @Test
    void should_get_3_when_knock_down_3_pins_in_1_throws() {
        bowlingScoreCalculator.throwBall(3);
        int expected = 3;
        assertEquals(expected, bowlingScoreCalculator.getTotalScore());
    }

    @Test
    void should_get_8_when_knock_down_3_pins_in_first_throw_and_5_pins_in_second_throw() {
        bowlingScoreCalculator.throwBall(3);
        bowlingScoreCalculator.throwBall(5);
        int expected = 8;
        assertEquals(expected, bowlingScoreCalculator.getTotalScore());
    }

    @Test
    void should_get_69_when_knock_down_without_spare_and_strike_in_10_frames() {
        int[] nums = {1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 6, 7, 3, 3, 2, 3, 4, 5, 3, 3};
        Arrays.stream(nums).forEach(v -> bowlingScoreCalculator.throwBall(v));
        int expected = 69;
        assertEquals(expected, bowlingScoreCalculator.getTotalScore());
    }

    @Test
    void should_get_20_when_first_frame_is_spare_and_next_throw_is_5() {
        int[] nums = {5, 5, 5};
        Arrays.stream(nums).forEach(v -> bowlingScoreCalculator.throwBall(v));
        int expected = 20;
        assertEquals(expected, bowlingScoreCalculator.getTotalScore());
    }

    @Test
    void should_get_61_when_some_frames_is_spare_but_last_one_is_not_and_no_strike() {
        int[] nums = {1, 2, 3, 5, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5};
        Arrays.stream(nums).forEach(v -> bowlingScoreCalculator.throwBall(v));
        int expected = 61;
        assertEquals(expected, bowlingScoreCalculator.getTotalScore());
    }
}