package com.twuc;

public class BowlingScoreCalculator {
    private int score;
    private boolean firstHit;
    private int firstHitNum;
    private boolean isSpare;

    public BowlingScoreCalculator() {
        firstHit = true;
    }

    public int getTotalScore() {
        return score;
    }

    public void throwBall(int num) {
        if (firstHit) {
            score = isSpare ? score += 2 * num : score + num;
            firstHit = false;
            firstHitNum = num;
        } else {
            firstHit = true;
            isSpare = (firstHitNum < 10 && (firstHitNum + num) == 10);
            score += num;
        }
    }
}
